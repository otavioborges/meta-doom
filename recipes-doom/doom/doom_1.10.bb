SUMMARY = "The canonical example of init scripts"
SECTION = "base"
DESCRIPTION = "This recipe is a canonical example of init scripts"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${WORKDIR}/COPYRIGHT;md5=349c872e0066155e1818b786938876a4"

DEPENDS = ""

SRC_URI = "git://github.com/makava/sdldoom-1.10-mod.git \
	  "
S = "${WORKDIR}/git"
